import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor() { }


  //method that uses local storage to save trainer name
  save (session: any){
    localStorage.setItem('trainer', JSON.stringify(session));
  }

  //Method to retrive trainer name
  get(): any{
    const saved = localStorage.getItem('trainer');
    return saved ? JSON.parse(saved) : false;
  }

  //Add pokemons to localStorage
  addPokemon(name){
    const saved = localStorage.getItem("pokemons");
    const pokemons = JSON.parse(saved);
    pokemons.push(name);
    localStorage.setItem("pokemons", JSON.stringify(pokemons));
  }
}
