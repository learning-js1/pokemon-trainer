import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from "../../../environments/environment"


@Injectable({
  providedIn: 'root'
})
export class PokeAPIService {

  constructor(private http: HttpClient) { }

  //Get pokemon by Name or ID
  getPokemon(name: string): Promise<any> {
    console.log(typeof(this.getPokemon))
    return this.http.get(`${environment.apiUrl}/${name}`).toPromise();
  }

  //Get first 20 pokemons
  getAll(): Promise<any>{
    return this.http.get(`${environment.apiUrl}`).toPromise();
  }

  //Get next 20 pokemons
  getNext(next: string): Promise<any>{
    return this.http.get(next).toPromise();
  }
}
