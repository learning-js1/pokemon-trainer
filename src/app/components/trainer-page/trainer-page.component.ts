import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer-page.component.html',
  styleUrls: ['./trainer-page.component.css']
})
export class TrainerPageComponent implements OnInit {

  constructor(private session: SessionService, private router: Router) {

    if (this.session.get() === ""){
      this.router.navigateByUrl("/register");
    }
   }
   public trainerName: string = ""; 
   public pokemons = [];



  ngOnInit(): void {
    this.trainerName = this.session.get().username;
    this.pokemons = JSON.parse(localStorage.getItem("pokemons"));
  }
}