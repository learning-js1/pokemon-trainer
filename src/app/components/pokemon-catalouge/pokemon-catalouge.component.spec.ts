import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonCatalougeComponent } from './pokemon-catalouge.component';

describe('PokemonCatalougeComponent', () => {
  let component: PokemonCatalougeComponent;
  let fixture: ComponentFixture<PokemonCatalougeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokemonCatalougeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokemonCatalougeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
