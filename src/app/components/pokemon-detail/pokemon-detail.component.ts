import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PokeAPIService } from 'src/app/services/pokeAPI/poke-api.service';
import { CommonModule } from '@angular/common';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.css']
})
export class PokemonDetailComponent implements OnInit {

  //Set up initial variables
  public name: string;
  public pokemon: any;

  constructor(private session: SessionService, private api: PokeAPIService,private route:ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.load();
  }

  //Async method that uses the api service to load the pokemon object
  async load(){
    try {
      this.name = this.route.snapshot.paramMap.get('name');
      this.pokemon = await this.api.getPokemon(this.name);
      console.log(this.pokemon);
    }catch(e){;
      console.log(e);
    }
  }

  //Adds the pokemon to the array of catched pokemons
  catch(){
    this.session.addPokemon(this.name);
    this.router.navigateByUrl("/trainer");

  }
  //Checks if pokemon is already owned.
  check(){
      const saved = localStorage.getItem("pokemons");
      const pokemons = JSON.parse(saved);
    if (pokemons.includes(this.name)){
      console.log("Pokemon is already catched");
      return false
    }else{
      console.log("Pokemon is not cathed");
      return true
    }
  }
}
