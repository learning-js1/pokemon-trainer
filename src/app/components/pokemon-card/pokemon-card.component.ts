import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { stringify } from 'querystring';
import { PokeAPIService } from 'src/app/services/pokeAPI/poke-api.service';

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.css']
})
export class PokemonCardComponent implements OnInit {

  public pokemon: any; 

  constructor(private api: PokeAPIService,private route:ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.load();
  }
  
  async load(){
    try{
      this.pokemon = await this.api.getAll();
      console.log("Next url: ", this.pokemon.next);
      console.log(this.pokemon);
    }catch(e){
      console.log(e);
    }
  }

  async loadNext(){
    try{
      this.pokemon = await this.api.getNext(this.pokemon.next);
    }catch(e){
      console.log(e);
    }
  }

}