import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-register-trainer',
  templateUrl: './register-trainer.component.html',
  styleUrls: ['./register-trainer.component.css']
})
export class RegisterTrainerComponent implements OnInit {

  constructor(private session: SessionService, private router: Router, private auth: AuthService) {
    if (this.session.get() !== false) {
      this.router.navigateByUrl("/trainer");
    }
  }



  ngOnInit(): void {
  }


  trainerForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.minLength(4)])
  })


  get username() {
    return this.trainerForm.get("username");
  }


  //Function that runs when register button is clicked.
  onRegisterClicked() {
    this.session.save({
      username: this.username.value
    });
    localStorage.setItem("pokemons", JSON.stringify([]));
    this.router.navigateByUrl("/trainer");
  }

}
