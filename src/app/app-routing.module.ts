import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PokemonCatalougeComponent } from './components/pokemon-catalouge/pokemon-catalouge.component';
import { PokemonDetailComponent } from './components/pokemon-detail/pokemon-detail.component';
import { RegisterTrainerComponent } from './components/register-trainer/register-trainer.component';
import { TrainerPageComponent } from './components/trainer-page/trainer-page.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: "register",
    component: RegisterTrainerComponent
  },
  {
    path: "trainer",
    component: TrainerPageComponent, 
    canActivate: [ AuthGuard ]
  },
  {
    path: "pokemon/:name",
    component: PokemonDetailComponent,
    canActivate: [ AuthGuard ]
  },
  {
    path: "catalouge",
    component: PokemonCatalougeComponent,
    canActivate: [ AuthGuard ]
  },
  {
    path: "",
    pathMatch: "full",
    redirectTo: "/register"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
